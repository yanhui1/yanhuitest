import { defineUserConfig } from 'vuepress'

export default defineUserConfig({
  lang: 'zh-CN',
  title: '你好， VuePress ！',
  base: '/yanhuitest/',
  description: '这是我的第一个 VuePress 站点',
  pagePatterns: ['**/*.md', '!.vuepress', '!node_modules', '!myBlog/*.md'],
  markdown:{
    anchor: false
  }
})