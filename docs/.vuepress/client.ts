import { defineClientConfig, usePageData } from '@vuepress/client'
// usePageData：可以拿到所有的页面信息
export default defineClientConfig({
  enhance({ app, router, siteData }) {
    const pagesData = usePageData()
    console.log('pagesData', usePageData().value)
   
    // usePageData返回的函数都是异步的
    // Promise.all(Object.keys(pagesData.value).map(
    //     key => pagesData.value[key]())).then(pages => {
    //         console.log(pages)
    //     }
    // )
    // console.log(pagesData.value)
  },
//   app: vuepress的应用实例
// router：vue-router的实例
// siteData：站点的配置信息
  setup() {},
  rootComponents: [],
//   rootComponents：注册全局组件
})